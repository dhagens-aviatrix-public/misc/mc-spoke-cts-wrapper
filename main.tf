#Create Aviatrix spoke resources
module "spoke" {
  for_each = { for k, v in var.services : k => v if v.node == "vpcs" } #Filter hack to overcome Consul filtering issue.

  source  = "terraform-aviatrix-modules/mc-spoke/aviatrix"
  version = "1.6.8"

  cloud   = each.value.meta.cloud
  name    = each.value.meta.name
  region  = each.value.meta.region
  account = each.value.meta.account
  //transit_gw       = 
  use_existing_vpc = true
  vpc_id           = each.value.meta.vpc_id
  gw_subnet        = each.value.meta.gateway_subnet_1
  hagw_subnet      = each.value.meta.gateway_subnet_2
  ha_gw            = false

  attached = false
}
